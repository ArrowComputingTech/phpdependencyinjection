<?php

  class Course {
    public $cid;
    public $lidObject;
    public $qidObject;

    function __construct($cid) {
      $this->cid = $cid;
    }
  }

  class Lesson extends Course {
    public $lid;
    
    function __construct($cid, Course $c) {
      $this->lid = $lid;
      $this->cid = $c->cid;
    }

    function showLessons() {
      echo "Here are the lessons: <br>";
    }

  }

  class Quiz extends Course {
    public $qid;

    function __construct($qid, Course $c) {
      $this->qid = $qid;
      $this->cid = $c->cid;
    }

    function showQuiz() {
      echo "Here are the questions: <br>";
    }
  }

  $course = new Course(1);
  $lesson = new Lesson(100, $course);
  $quiz = new Quiz(200, $course);

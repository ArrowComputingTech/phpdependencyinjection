<?php

  class University {
    public $name;
  }

  class Student {
    public $name;
    public $id;
    public $university;
      
    function __construct($name, $id, University $uni) {
      $this->name = $name;
      $this->id = $id;
      $this->university = $uni->name;
      echo "{$this->name} (ID {$this->id}) created, attending {$this->university}<br>";
    }
  }

  $uni1 = new University;
  $uni1->name = "Ouachita Baptist University";
  $stu1 = new Student("Smith, John", 42, $uni1);

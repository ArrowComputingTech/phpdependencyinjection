<?php

  class Course {
    public $cid;
    public $lidObject;
    public $qidObject;

    function __construct($cid, $lid, $qid) {
      $this->cid = $cid;
      $this->lidObject = new Lesson($lid);
      $this->qidObject = new Quiz($qid);
    }
  }

  class Lesson extends Course {
    public $lid;
    
    function __construct($cid) {
      $this->lid = $lid;
    }

    function showLessons() {
      echo "Here are the lessons: <br>";
    }

  }

  class Quiz extends Course {
    public $qid;

    function __construct($qid) {
      $this->qid = $qid;
    }

    function showQuiz() {
      echo "Here are the questions: <br>";
    }
  }

  $course = new Course(1, 100, 200);
  $course->$lidObject->showLessons();
  $course->$qidObject->showQuiz();
